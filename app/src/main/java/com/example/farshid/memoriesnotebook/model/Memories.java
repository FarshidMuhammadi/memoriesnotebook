package com.example.farshid.memoriesnotebook.model;

import java.util.ArrayList;

/**
 * Created by Farshid on 4/18/2017.
 */

public class Memories {
    private ArrayList<Memory> mMemories;


    public Memories(ArrayList<Memory> memories){
        this.mMemories=memories;

    }

    public ArrayList<Memory> getMemories() {
        return mMemories;
    }

    public void setMemories(ArrayList<Memory> mMemories) {
        this.mMemories = mMemories;
    }

    public void addMemories(Memory mMemories) {
        this.mMemories.add(mMemories);
    }
}
