package com.example.farshid.memoriesnotebook.controller;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.farshid.memoriesnotebook.R;
import com.example.farshid.memoriesnotebook.model.Memory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class ShowActivity extends AppCompatActivity {


    private EditText title, date, location, description;
    private ImageView imageViewImage,deleteImageView,editImageView,uploadImageView;
    private Button edit;
    private  int position;
    public static final String DELETE_POSITION="DELETE_POSITION";
    public static final int DELETE_RESULT_CODE=7;
    private RelativeLayout relativeLayout;
    private String mPhotoPath;
    private boolean isEditable=false;
    private  Memory memory;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        title = (EditText) findViewById(R.id.title_show);
        date = (EditText) findViewById(R.id.date_show);
        location = (EditText) findViewById(R.id.location_show);
        description = (EditText) findViewById(R.id.description_show);
        imageViewImage = (ImageView) findViewById(R.id.photo_show);
        deleteImageView=(ImageView) findViewById(R.id.delete_image_view) ;
        relativeLayout=(RelativeLayout)findViewById(R.id.content_show);
        editImageView=(ImageView) findViewById(R.id.edit_image_view);
        edit=(Button)findViewById(R.id.show_edit_button);
        uploadImageView=(ImageView) findViewById(R.id.upload_imageView);



        makeEditable(false);

        edit.setVisibility(View.INVISIBLE);


        Intent intent = getIntent();
         memory = (Memory) intent.getParcelableExtra(MainActivity.MEMORY_OBJECT_TO_SHOW);
        position=intent.getIntExtra(MainActivity.POSITION,-1);

        title.setText(memory.getTitle());
        date.setText(memory.getDate());
        createBitmap(memory.getPhotoPath());
        location.setText(memory.getLocation());
        description.setText(memory.getDescription());

        mPhotoPath=memory.getPhotoPath();


        deleteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    Intent intent1=new Intent();
                intent1.putExtra(DELETE_POSITION,position);
                setResult(DELETE_RESULT_CODE, intent1);
                clearFields();

                makeAlertDialog("Your memory has been Delete",true).show();
            }
        });

        editImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isEditable=true;
                edit.setVisibility(View.VISIBLE);
                makeEditable(true);



            }
        });

        uploadImageView.setOnClickListener(new View.OnClickListener() {



            public void onClick(View v) {

                writeMemoryToFile();



            }
        });





        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Memory memory=getMemory();

                Intent editIntent=new Intent();
                editIntent.putExtra("editedMemory",memory);
                editIntent.putExtra("Editposition",position);
                setResult(61,editIntent);
              finish();


            }
        });


        imageViewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isEditable) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    setResult(81,intent);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), 82);
                }
            }
        });

    }

    private void writeMemoryToFile() {

        try {
            FileOutputStream fileOutputStream= openFileOutput(memory.getTitle()+" Memory"+".txt",MODE_WORLD_READABLE);
            OutputStreamWriter outputStreamWriter=new OutputStreamWriter(fileOutputStream);

            outputStreamWriter.write("Title:"+memory.getTitle()+"\n     ");
            outputStreamWriter.write("Date:"+memory.getDate()+"\n       ");
            outputStreamWriter.write("Location:"+memory.getLocation()+"\n       ");
            outputStreamWriter.write("Description:"+memory.getDescription()+"\n     ");
            outputStreamWriter.close();


            makeAlertDialog("your memory has been exported",false).show();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if (requestCode == 82 && resultCode ==RESULT_OK&& data != null && data.getData() != null) {



            Uri uri = data.getData();

            //getting path of photo
            String[] projection = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(projection[0]);
            String inputPath = cursor.getString(columnIndex); // returns null
            cursor.close();

            String inputFileName=getFileName(uri);

            Log.d("FarshidM",inputPath);

            File outputPath= getApplicationContext().getFilesDir();
            Log.d("outputpath",outputPath.getPath());

            copyFile(inputPath,inputFileName,outputPath.getPath());

//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                createBitmap(inputPath);
//                imageViewImage.setImageBitmap(bitmap);
                imageViewImage.setVisibility(View.VISIBLE);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }


    }


    //copy file from gallery to inter storage file system
    private void copyFile(String inputPath, String inputFile, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File (outputPath);
            if (!dir.exists())
            {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath);
            out = new FileOutputStream(outputPath + inputFile);

            Log.d("absOutPath",outputPath + inputFile);
            //path of final image
            mPhotoPath=outputPath + inputFile;

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file (You have now copied the file)
            out.flush();
            out.close();
            out = null;

        }  catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }
        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }


    //extracting the name of file
    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


    public void createBitmap(String path){


//        File image = new File(path);
//        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(path);

        imageViewImage.setImageBitmap(bitmap);

    }


         public  void clearFields(){

         title.setText("");
         date.setText("");
         location.setText("");
         description.setText("");
         imageViewImage.setVisibility(View.INVISIBLE);


     }

    public AlertDialog.Builder makeAlertDialog(String message, final boolean toFinish){
        AlertDialog.Builder alerBuilder=new AlertDialog.Builder(ShowActivity.this);
        alerBuilder.setTitle("Info");
        alerBuilder.setMessage(message);


        alerBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(toFinish){
                finish();
                }
            }
        });

        alerBuilder.create();

        return alerBuilder;
    }

    private void makeEditable(boolean flag){
        title.setEnabled(flag);
        date.setEnabled(flag);
        location.setEnabled(flag);
        description.setEnabled(flag);

    }

    //setting data to the memory model
    public Memory getMemory(){
        Memory memory=new Memory();
        memory.setPhotoPath(mPhotoPath);
        memory.setTitle(title.getText().toString());
        memory.setDate(date.getText().toString());
        memory.setLocation(location.getText().toString());
        memory.setDescription(description.getText().toString());

        return memory;

    }





}