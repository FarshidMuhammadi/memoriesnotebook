package com.example.farshid.memoriesnotebook.controller;

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.farshid.memoriesnotebook.R;
import com.example.farshid.memoriesnotebook.database.DBOperation;

import com.example.farshid.memoriesnotebook.model.Memories;
import com.example.farshid.memoriesnotebook.model.Memory;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{

    private Toolbar toolbar;
    private ImageView imageViewAdd;
    private ListView mlistView;
    private MenuItem item;
    Memories memories;
    private ArrayList<String> ListItemes;
    private final int MY_REQUEST_CODE=44;
    public static final int ADD_RESULT_OK =55;
    public static final int EDIT_RESULT_OK =34;
    public static final String MEMORY_OBJECT_TO_SHOW="MEMORY_OBJECT_TO_SHOW";
    public static final String POSITION="POSITION";
    private ArrayList<Memory> mMemoryArrayList;
    public final int DELETE_OR_EDIT_REQUEST_CODE=9;
    private MemoryAdapter mMemoryAdapter;
    private SearchView actionSearch;
    private DBOperation dbOperation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        toolbar= (Toolbar) findViewById(R.id.toolbar);
        imageViewAdd= (ImageView) findViewById(R.id.action_add_imageView);
        mlistView=(ListView) findViewById(R.id.main_listView);


        dbOperation=new DBOperation(getApplicationContext());

//get memories from database
        mMemoryArrayList=dbOperation.getMemories();

             mMemoryAdapter =new MemoryAdapter(this,mMemoryArrayList);

        mlistView.setAdapter(mMemoryAdapter);


        //add menu to the toolbar
        setSupportActionBar(toolbar);

        imageViewAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,AddActivity.class);
                setResult(ADD_RESULT_OK);
                startActivityForResult(intent,MY_REQUEST_CODE);

            }
        });


        //add listener to the itemes of list view

        mlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent=new Intent(MainActivity.this,ShowActivity.class);
                intent.putExtra(MEMORY_OBJECT_TO_SHOW,mMemoryArrayList.get(position));
                intent.putExtra(POSITION,position);
                startActivityForResult(intent, DELETE_OR_EDIT_REQUEST_CODE);

            }
        });


    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == MY_REQUEST_CODE && resultCode == ADD_RESULT_OK && data != null) {

            Memory memory = (Memory) data.getParcelableExtra(AddActivity.MEMORY_ARRAY_LIST);

                mMemoryArrayList.add(memory);

            //adding memory to the database
            dbOperation.insertMemory(memory);


            mMemoryAdapter =new MemoryAdapter(this,mMemoryArrayList);
            mlistView.setAdapter(mMemoryAdapter);


        } else if(requestCode == DELETE_OR_EDIT_REQUEST_CODE && resultCode ==ShowActivity.DELETE_RESULT_CODE && data != null){

            int position=(int)data.getIntExtra(ShowActivity.DELETE_POSITION,-1);
            mMemoryArrayList.remove(position);
            dbOperation.deleteMemory(position+1);


            mMemoryAdapter =new MemoryAdapter(this,mMemoryArrayList);
            mlistView.setAdapter(mMemoryAdapter);



        }else if(requestCode==DELETE_OR_EDIT_REQUEST_CODE && resultCode==61 && data!=null){

            int position =(int) data.getIntExtra("Editposition",-1);

            Memory memory=(Memory) data.getParcelableExtra("editedMemory");
            mMemoryArrayList.set(position,memory);

            dbOperation.updateMemory(position+1,memory);

            Log.d("editresultCode","="+position +"-"+memory.getTitle());

            mMemoryAdapter =new MemoryAdapter(this,mMemoryArrayList);
            mlistView.setAdapter(mMemoryAdapter);



        }

            }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.my_menu,menu);

        item =menu.findItem(R.id.action_search);

         actionSearch = (SearchView) MenuItemCompat.getActionView(item);
        actionSearch.setOnQueryTextListener(this);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.action_setting){
            Toast.makeText(MainActivity.this,"Setting has been perssed", Toast.LENGTH_LONG).show();
        }else if(item.getItemId()==R.id.action_about_us){
            Toast.makeText(MainActivity.this,"About us ", Toast.LENGTH_LONG).show();
        }

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        return  false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        mMemoryAdapter.getFilter().filter(newText);

        return false;
    }
}
