package com.example.farshid.memoriesnotebook.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Farshid on 4/22/2017.
 */

public class DatabaseConfig extends SQLiteOpenHelper {

    public static final String  DATABASE_NAME="MemoriesNotebook.db";
    public static final int  DATABASE_VERSION=1;

    public static final  String TABLE_MEMORY_NAME ="Memory";

    public static final String MEMORY_COLUMN_NAME_ID="_id";
    public static final String MEMORY_COLUMN_NAME_TITLE="title";
    public static final String MEMORY_COLUMN_NAME_DATE ="date";
    public static final String MEMORY_COLUMN_NAME_LOCATION="location";
    public static final String MEMORY_COLUMN_NAME_DESCRIPTION="description";
    public static final String MEMORY_COLUMN_NAME_PHOTOPATH="photoPath";


    private static final String SQL_CREATE_TABLE_MEMORY="CREATE TABLE "+TABLE_MEMORY_NAME+" ( "+MEMORY_COLUMN_NAME_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"+MEMORY_COLUMN_NAME_TITLE+" TEXT , "+ MEMORY_COLUMN_NAME_DATE +" TEXT , "+
            MEMORY_COLUMN_NAME_LOCATION+" TEXT,"+MEMORY_COLUMN_NAME_DESCRIPTION+" TEXT,"+MEMORY_COLUMN_NAME_PHOTOPATH+" TEXT )";
    private static final String SQL_DELETE_TABLE_MEMORY="DROP TABLE IF EXIST "+TABLE_MEMORY_NAME+"";

    public DatabaseConfig(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_MEMORY);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE_MEMORY);
        onCreate(db);
    }

}
