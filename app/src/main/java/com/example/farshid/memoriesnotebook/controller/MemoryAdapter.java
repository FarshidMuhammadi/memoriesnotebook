package com.example.farshid.memoriesnotebook.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.farshid.memoriesnotebook.R;
import com.example.farshid.memoriesnotebook.model.Memory;

import java.util.ArrayList;

/**
 * Created by Farshid on 4/20/2017.
 */

public class MemoryAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private ArrayList<Memory> memories;
    ValueFilter valueFilter;
    private ArrayList<Memory> mStringFilterList;


    public MemoryAdapter(Context context, ArrayList<Memory> items) {

        this.context = context;
        this.memories = items;
        mStringFilterList=items;
    }

    @Override
    public int getCount() {
        return memories.size();
    }

    @Override
    public Object getItem(int position) {
        return memories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
        }


        Memory currentMemory = (Memory) getItem(position);


        TextView textViewTitle = (TextView) convertView.findViewById(R.id.title_item);
        TextView textViewDate = (TextView) convertView.findViewById(R.id.date_item);

        textViewTitle.setText(currentMemory.getTitle());
        textViewDate.setText(currentMemory.getDate());


        return convertView;
    }

    @Override
    public Filter getFilter() {

        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }

        return valueFilter;
    }


    private class ValueFilter extends Filter {


        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if(constraint!=null && constraint.length()>0){
                ArrayList<Memory> filterList = new ArrayList<Memory>();


                for(int i=0;i<mStringFilterList.size();i++){
                    if(mStringFilterList.get(i).getTitle().toUpperCase().contains(constraint.toString().toUpperCase())){
                            Memory memory=new Memory();
                        memory.setTitle(mStringFilterList.get(i).getTitle());
                        memory.setDate(mStringFilterList.get(i).getDate());
                        memory.setLocation(mStringFilterList.get(i).getLocation());
                        memory.setDescription(mStringFilterList.get(i).getDescription());
                        memory.setPhotoPath(mStringFilterList.get(i).getPhotoPath());

                        filterList.add(memory);
                    }

                }

                results.count=filterList.size();

                results.values=filterList;


            }
            else{
                results.count=mStringFilterList.size();
                results.values=mStringFilterList;

            }


            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            memories=(ArrayList<Memory>)results.values;
            notifyDataSetChanged();
        }
    }


}