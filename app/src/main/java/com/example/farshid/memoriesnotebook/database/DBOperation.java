package com.example.farshid.memoriesnotebook.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.example.farshid.memoriesnotebook.model.Memory;

import java.util.ArrayList;

/**
 * Created by Farshid on 4/22/2017.
 */

public class DBOperation {

    private DatabaseConfig databaseConfig;


    public DBOperation(Context context){
        databaseConfig=new DatabaseConfig(context);
        databaseConfig.getWritableDatabase();
    }

    public void insertMemory(Memory memory){
        SQLiteDatabase database=databaseConfig.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

        contentValues.put(DatabaseConfig.MEMORY_COLUMN_NAME_TITLE,memory.getTitle());
        contentValues.put(DatabaseConfig.MEMORY_COLUMN_NAME_LOCATION,memory.getLocation());
        contentValues.put(DatabaseConfig.MEMORY_COLUMN_NAME_DATE,memory.getDate());
        contentValues.put(DatabaseConfig.MEMORY_COLUMN_NAME_DESCRIPTION,memory.getDescription());
        contentValues.put(DatabaseConfig.MEMORY_COLUMN_NAME_PHOTOPATH,memory.getPhotoPath());

        database.insert(DatabaseConfig.TABLE_MEMORY_NAME,null,contentValues);
        database.close();

    }

    public int numberOfRows(){
        SQLiteDatabase db = databaseConfig.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, DatabaseConfig.TABLE_MEMORY_NAME);
        return numRows;
    }


    public ArrayList<Memory> getMemories(){
        ArrayList<Memory> memories=new ArrayList<Memory>();
        SQLiteDatabase database=databaseConfig.getWritableDatabase();

        String columns []={DatabaseConfig.MEMORY_COLUMN_NAME_TITLE,DatabaseConfig.MEMORY_COLUMN_NAME_DATE,DatabaseConfig.MEMORY_COLUMN_NAME_LOCATION,
        DatabaseConfig.MEMORY_COLUMN_NAME_DESCRIPTION,DatabaseConfig.MEMORY_COLUMN_NAME_PHOTOPATH};

        Cursor cursor=database.query(DatabaseConfig.TABLE_MEMORY_NAME,columns,null,null,null,null,null);



        while (cursor.moveToNext()){
            Memory memory=new Memory();

            memory.setTitle(cursor.getString(cursor.getColumnIndex(DatabaseConfig.MEMORY_COLUMN_NAME_TITLE)));
            memory.setDate(cursor.getString(cursor.getColumnIndex(DatabaseConfig.MEMORY_COLUMN_NAME_DATE)));
            memory.setLocation(cursor.getString(cursor.getColumnIndex(DatabaseConfig.MEMORY_COLUMN_NAME_LOCATION)));
            memory.setDescription(cursor.getString(cursor.getColumnIndex(DatabaseConfig.MEMORY_COLUMN_NAME_DESCRIPTION)));
            memory.setPhotoPath(cursor.getString(cursor.getColumnIndex(DatabaseConfig.MEMORY_COLUMN_NAME_PHOTOPATH)));

            memories.add(memory);
        }

        cursor.close();
        database.close();


        return memories;
    }

    //update memory by id
    public void  updateMemory(int row,Memory memory){
            SQLiteDatabase database=databaseConfig.getWritableDatabase();

        ContentValues contentValues=new ContentValues();
        contentValues.put(DatabaseConfig.MEMORY_COLUMN_NAME_TITLE,memory.getTitle());
        contentValues.put(DatabaseConfig.MEMORY_COLUMN_NAME_DATE,memory.getDate());
        contentValues.put(DatabaseConfig.MEMORY_COLUMN_NAME_LOCATION,memory.getLocation());
        contentValues.put(DatabaseConfig.MEMORY_COLUMN_NAME_DESCRIPTION,memory.getDescription());
        contentValues.put(DatabaseConfig.MEMORY_COLUMN_NAME_PHOTOPATH,memory.getPhotoPath());

        String whereClause=DatabaseConfig.MEMORY_COLUMN_NAME_ID + " =? ";
        String whereArg[]={row+""};

        database.update(DatabaseConfig.TABLE_MEMORY_NAME,contentValues,whereClause,whereArg);
        database.close();

    }

    public void deleteMemory(int row){
        SQLiteDatabase database=databaseConfig.getWritableDatabase();

        String whereClause=DatabaseConfig.MEMORY_COLUMN_NAME_ID + " =? ";
        String whereArg[]={row+""};

        database.delete(DatabaseConfig.TABLE_MEMORY_NAME,whereClause,whereArg);
        database.close();

    }




}
