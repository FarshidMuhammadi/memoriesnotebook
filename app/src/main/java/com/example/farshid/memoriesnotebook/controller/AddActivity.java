package com.example.farshid.memoriesnotebook.controller;

import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.farshid.memoriesnotebook.R;
import com.example.farshid.memoriesnotebook.model.Memories;
import com.example.farshid.memoriesnotebook.model.Memory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class AddActivity extends AppCompatActivity {

    private ImageView mSelectedImageImageView;
    private EditText mTileEditText,mDateEditText,mLocationEditText,mDescriptionEditText;
    private Button selectPhotoButton;
    private String mPhotoPath;
    public static final String MEMORY_ARRAY_LIST="MEMORY_ARRAY_LIST";
    private final int MY_PICK_IMAGE_REQUEST = 11;
    private final int MY_RESULT_OK_IMAGE_CHOOSER=12;


    private FloatingActionButton fab;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        selectPhotoButton =(Button) findViewById(R.id.photo_button_add);
        mTileEditText=(EditText) findViewById(R.id.title_editText_add);
        mDateEditText=(EditText) findViewById(R.id.date_editText_add);
        mLocationEditText=(EditText) findViewById(R.id.location_editText_add);
        mDescriptionEditText=(EditText) findViewById(R.id.description_editText_add);
        mSelectedImageImageView = (ImageView) findViewById(R.id.selected_photo_add);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        mSelectedImageImageView.setVisibility(View.INVISIBLE);


        selectPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent =new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                setResult(MY_RESULT_OK_IMAGE_CHOOSER);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), MY_PICK_IMAGE_REQUEST);

            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Memory memory= getMemory();

                    Intent intent =new Intent();
                    intent.putExtra(MEMORY_ARRAY_LIST,memory);
                    setResult(MainActivity.ADD_RESULT_OK, intent);
                    finish();



            }
        });








    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        Log.d("isStart  2","Yes"+MY_PICK_IMAGE_REQUEST+"  "+MY_RESULT_OK_IMAGE_CHOOSER);

        if (requestCode == MY_PICK_IMAGE_REQUEST&& resultCode ==RESULT_OK && data != null && data.getData() != null) {



            Uri uri = data.getData();

            //getting path of photo
            String[] projection = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
            cursor.moveToFirst();

            Log.d("Farshid", DatabaseUtils.dumpCursorToString(cursor));

            int columnIndex = cursor.getColumnIndex(projection[0]);
            String inputPath = cursor.getString(columnIndex); // returns null
            cursor.close();

            String inputFileName=getFileName(uri);

            Log.d("FarshidM",inputPath);

           File outputPath= getApplicationContext().getFilesDir();
            Log.d("outputpath",outputPath.getPath());

            copyFile(inputPath,inputFileName,outputPath.getPath());

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));


                mSelectedImageImageView.setImageBitmap(bitmap);
                mSelectedImageImageView.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //extracting the name of file
    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


    //copy file from gallery to inter storage file system
    private void copyFile(String inputPath, String inputFile, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File (outputPath);
            if (!dir.exists())
            {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath);
            out = new FileOutputStream(outputPath + inputFile);

            Log.d("absOutPath",outputPath + inputFile);
            //path of final image
            mPhotoPath=outputPath + inputFile;

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file (You have now copied the file)
            out.flush();
            out.close();
            out = null;

        }  catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }
        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }

    //setting data to the memory model
    public Memory getMemory(){
        Memory memory=new Memory();
        memory.setPhotoPath(mPhotoPath);
        memory.setTitle(mTileEditText.getText().toString());
        memory.setDate(mDateEditText.getText().toString());
        memory.setLocation(mLocationEditText.getText().toString());
        memory.setDescription(mDescriptionEditText.getText().toString());

        return memory;

    }






//    public void createBitmap(String path,ImageView imageView){
//
//
//
//
//        Bitmap bitmap = BitmapFactory.decodeFile(path);
//
//        imageView.setImageBitmap(bitmap);
//        imageView.setVisibility(View.VISIBLE);
//
//    }

//     public  void clearEditText(){
//         mTileEditText.setText("");
//         mDateEditText.setText("");
//         mLocationEditText.setText("");
//         mDescriptionEditText.setText("");
//         mSelectedImageImageView.setVisibility(View.INVISIBLE);
//
//
//     }


//    public AlertDialog.Builder makeAlertDialog(){
//        AlertDialog.Builder alerBuilder=new AlertDialog.Builder(AddActivity.this);
//        alerBuilder.setTitle("Info");
//        alerBuilder.setMessage("Your memory has been saved");
//
//
//        alerBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//            }
//        });
//
//        alerBuilder.create();
//
//        return alerBuilder;
//    }


}
