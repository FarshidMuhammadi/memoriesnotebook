package com.example.farshid.memoriesnotebook.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Farshid on 4/17/2017.
 */

public class Memory implements Parcelable{


    private String title;
    private String date;
    private String location;
    private String description;
    private String photoPath;



    public Memory(){

    }


    protected Memory(Parcel in) {
        title = in.readString();
        date = in.readString();
        location = in.readString();
        description = in.readString();
        photoPath = in.readString();
    }

    public static final Creator<Memory> CREATOR = new Creator<Memory>() {
        @Override
        public Memory createFromParcel(Parcel in) {
            return new Memory(in);
        }

        @Override
        public Memory[] newArray(int size) {
            return new Memory[size];
        }
    };

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(date);
        dest.writeString(location);
        dest.writeString(description);
        dest.writeString(photoPath);
    }
}
